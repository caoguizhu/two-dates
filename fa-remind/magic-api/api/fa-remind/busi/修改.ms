{
  "properties" : { },
  "id" : "930d8c4140f0415cb1542cc25d3b0e5a",
  "script" : null,
  "groupId" : "c8e5322255674d9aae91d7f99771d517",
  "name" : "修改",
  "createTime" : null,
  "updateTime" : 1698512408278,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "update",
  "method" : "PUT",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "{\r\n    \"realityTime\":\"2023-10-29 00:54:12\",\r\n  \r\n    \"id\":75\r\n}",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": -1,\n    \"message\": \"系统内部出现错误\",\n    \"data\": null,\n    \"timestamp\": 1698512367761,\n    \"executeTime\": 24\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.update(
    """
   UPDATE `fa_remind`.`fa_busi` 
   SET  `update_time` =now()
   ?{body.crewUserId, ,`crew_user_id` = #{body.crewUserId}}
   ?{body.crewUserName, ,`crew_user_name` = #{body.crewUserName}}
   ?{body.crewUserPhone, ,`crew_user_phone` = #{body.crewUserPhone}}
   ?{body.realityTime, ,`reality_time` = #{body.realityTime}}
   ?{body.phoneRemindTime, ,`phone_remind_time` = #{body.phoneRemindTime}}
   WHERE `id` = #{body.id};
    """
)
//   -- ?{body.crewUserId ,`crew_user_id` = #{body.crewUserId}}
//     -- ?{body.crewUserName ,`crew_user_name` = #{body.crewUserName}}
//     -- ?{body.crewUserPhone ,`crew_user_phone` = #{body.crewUserPhone}}
//     -- ?{body.realityTime ,`reality_time` = #{body.realityTime}}
//     -- ?{body.phoneRemindTime ,`phone_remind_time` = #{body.phoneRemindTime}}
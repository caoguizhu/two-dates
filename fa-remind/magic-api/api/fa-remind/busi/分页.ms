{
  "properties" : { },
  "id" : "abd1f635cda542488d275315ee3bdf35",
  "script" : null,
  "groupId" : "c8e5322255674d9aae91d7f99771d517",
  "name" : "分页",
  "createTime" : null,
  "updateTime" : 1698528402900,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "page",
  "method" : "GET",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : null,
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.page("""
    select id,train_number,station_name,operation_time,report_time,crew_user_id,crew_user_name,crew_user_phone,
    DATE_FORMAT(reality_time, '%Y-%m-%d %H:%i:%s' ) as reality_time,
    DATE_FORMAT(phone_remind_time, '%Y-%m-%d %H:%i:%s' ) as phone_remind_time,
    DATE_FORMAT(create_time, '%Y-%m-%d %H:%i:%s' ) as create_time,
    DATE_FORMAT(update_time, '%Y-%m-%d %H:%i:%s' ) as update_time
    from fa_busi 
    where DATE(create_time) = CURDATE() 
    ?{endTime , and create_time <= #{endTime}}
    ?{startTime , and create_time >= #{startTime}}
    ORDER BY `train_number` ASC
    
""")
// BETWEEN '2023-01-01' AND '2023-12-31';
{
  "properties" : { },
  "id" : "7af84696b881466bbdfa8fe67881e5d3",
  "script" : null,
  "groupId" : "c8e5322255674d9aae91d7f99771d517",
  "name" : "查询",
  "createTime" : null,
  "updateTime" : 1698528417854,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "list",
  "method" : "GET",
  "parameters" : [ {
    "name" : "size",
    "value" : "10",
    "description" : null,
    "required" : false,
    "dataType" : "String",
    "type" : null,
    "defaultValue" : null,
    "validateType" : null,
    "error" : null,
    "expression" : null,
    "children" : null
  }, {
    "name" : "current",
    "value" : "1",
    "description" : null,
    "required" : false,
    "dataType" : "String",
    "type" : null,
    "defaultValue" : null,
    "validateType" : null,
    "error" : null,
    "expression" : null,
    "children" : null
  } ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": 1,\n    \"message\": \"success\",\n    \"data\": {\n        \"total\": 4,\n        \"list\": [\n            {\n                \"id\": 78,\n                \"trainNumber\": \"K1234\",\n                \"stationName\": \"兰州\",\n                \"operationTime\": \"12到，13开\",\n                \"reportTime\": \"00:03:00\",\n                \"crewUserId\": 4,\n                \"crewUserName\": \"陈菁12\",\n                \"crewUserPhone\": \"13909425614\",\n                \"realityTime\": \"2023-10-29 01:46:10\",\n                \"phoneRemindTime\": \"2023-10-29 01:32:41\",\n                \"createTime\": \"2023-10-29 00:24:47\",\n                \"updateTime\": \"2023-10-29 01:46:10\"\n            },\n            {\n                \"id\": 79,\n                \"trainNumber\": \"k2345\",\n                \"stationName\": \"环县\",\n                \"operationTime\": \"15到，17开\",\n                \"reportTime\": \"00:15:00\",\n                \"crewUserId\": 4,\n                \"crewUserName\": \"陈菁12\",\n                \"crewUserPhone\": \"13909425614\",\n                \"realityTime\": \"2023-10-29 01:47:59\",\n                \"phoneRemindTime\": \"2023-10-29 01:47:24\",\n                \"createTime\": \"2023-10-29 00:24:47\",\n                \"updateTime\": \"2023-10-29 01:47:59\"\n            },\n            {\n                \"id\": 80,\n                \"trainNumber\": \"K1234\",\n                \"stationName\": \"兰州\",\n                \"operationTime\": \"12到，13开\",\n                \"reportTime\": \"00:03:00\",\n                \"crewUserId\": 18,\n                \"crewUserName\": \"曹贵珠\",\n                \"crewUserPhone\": \"13909425614\",\n                \"realityTime\": \"2023-10-29 01:47:16\",\n                \"phoneRemindTime\": \"2023-10-29 01:46:22\",\n                \"createTime\": \"2023-10-29 00:24:47\",\n                \"updateTime\": \"2023-10-29 01:47:16\"\n            },\n            {\n                \"id\": 81,\n                \"trainNumber\": \"k2345\",\n                \"stationName\": \"环县\",\n                \"operationTime\": \"15到，17开\",\n                \"reportTime\": \"00:15:00\",\n                \"crewUserId\": 18,\n                \"crewUserName\": \"曹贵珠\",\n                \"crewUserPhone\": \"13909425614\",\n                \"realityTime\": \"2023-10-29 01:47:04\",\n                \"phoneRemindTime\": \"2023-10-29 01:48:31\",\n                \"createTime\": \"2023-10-29 00:24:47\",\n                \"updateTime\": \"2023-10-29 01:48:31\"\n            }\n        ]\n    },\n    \"timestamp\": 1698515480487,\n    \"executeTime\": 12\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.select("""
    select id,train_number,station_name,operation_time,report_time,crew_user_id,crew_user_name,crew_user_phone,
    DATE_FORMAT(reality_time, '%Y-%m-%d %H:%i:%s' ) as reality_time,
    DATE_FORMAT(phone_remind_time, '%Y-%m-%d %H:%i:%s' ) as phone_remind_time,
    DATE_FORMAT(create_time, '%Y-%m-%d %H:%i:%s' ) as create_time,
    DATE_FORMAT(update_time, '%Y-%m-%d %H:%i:%s' ) as update_time
    from fa_busi 
    where DATE(create_time) = CURDATE() 
    ?{endTime , and create_time <= #{endTime}}
    ?{startTime , and create_time >= #{startTime}}
    ORDER BY `train_number` ASC
    
""")
// BETWEEN '2023-01-01' AND '2023-12-31';
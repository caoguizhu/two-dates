{
  "properties" : { },
  "id" : "2e9108f68f9a451ebc4426bfbe0890ce",
  "script" : null,
  "groupId" : "c8e5322255674d9aae91d7f99771d517",
  "name" : "绑定人员",
  "createTime" : null,
  "updateTime" : 1698394803571,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "bindUser",
  "method" : "POST",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "{\r\n    \"trainId\":\"1\",\r\n    \"userId\":\"1\",\r\n    \"userName\":\"12\",\r\n    \"trainNumber\":\"12\",\r\n    \"stationName\":\"12\",\r\n    \"operationTime\":\"12\",\r\n    \"reportTime\":\"12\"\r\n}",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": -1,\n    \"message\": \"系统内部出现错误\",\n    \"data\": null,\n    \"timestamp\": 1698394792724,\n    \"executeTime\": 6\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.insert(
    """
   INSERT INTO `fa_remind`.`fa_busi`(`train_id`, `user_id`,`user_name`, `train_number`, `station_name`, `operation_time`, `report_time`,`create_time`) 
   VALUES (#{body.trainId},#{body.userId},#{body.userName},#{body.trainNumber},#{body.stationName},#{body.operationTime},#{body.reportTime}, now());
    """
)
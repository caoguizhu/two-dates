{
  "properties" : { },
  "id" : "5cf9647f11cd488cbe3accc1bd97dbbb",
  "script" : null,
  "groupId" : "bac6f989d5f94a149eeac4bad9ff0b24",
  "name" : "保存",
  "createTime" : null,
  "updateTime" : 1698392495208,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "save",
  "method" : "POST",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "{\r\n    \"number\": \"1\",\r\n    \"stationName\":\" 2\",\r\n    \"operationTime\": \"3\",\r\n    \"reportTime\":\"4\"\r\n}",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": -1,\n    \"message\": \"系统内部出现错误\",\n    \"data\": null,\n    \"timestamp\": 1698392463312,\n    \"executeTime\": 6\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.insert(
    """
    INSERT INTO `fa_remind`.`fa_train`(`number`, `station_name`, `operation_time`, `report_time`, `create_time`) 
    VALUES ( #{body.number}, #{body.stationName}, #{body.operationTime}, #{body.reportTime}, now());
    """
)
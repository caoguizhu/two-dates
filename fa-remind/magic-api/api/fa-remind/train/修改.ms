{
  "properties" : { },
  "id" : "6c44fe26083943b783e6b0a288634f5f",
  "script" : null,
  "groupId" : "bac6f989d5f94a149eeac4bad9ff0b24",
  "name" : "修改",
  "createTime" : null,
  "updateTime" : 1698512570802,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "update",
  "method" : "PUT",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": -1,\n    \"message\": \"系统内部出现错误\",\n    \"data\": null,\n    \"timestamp\": 1698512562860,\n    \"executeTime\": 17\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.update(
    """
   UPDATE `fa_remind`.`fa_train` 
   SET  `update_time` =now()
    ?{body.number, ,`number` = #{body.number}}
     ?{body.stationName, ,`station_name` = #{body.stationName}}
      ?{body.operationTime, ,`operation_time` = #{body.operationTime}}
      ?{body.reportTime, ,`report_time` =  #{body.reportTime}}
   WHERE `id` = #{body.id};
    """
)
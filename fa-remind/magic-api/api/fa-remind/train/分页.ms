{
  "properties" : { },
  "id" : "862053f2c2f845568a8e4c882d0ae198",
  "script" : null,
  "groupId" : "bac6f989d5f94a149eeac4bad9ff0b24",
  "name" : "分页",
  "createTime" : null,
  "updateTime" : 1698423095040,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "page",
  "method" : "GET",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : null,
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.page("""
    select `id`, `number`, `station_name`, `operation_time`, 
    `report_time`, DATE_FORMAT(create_time, '%Y-%m-%d %H:%i:%s' ) as create_time,
    DATE_FORMAT(update_time, '%Y-%m-%d %H:%i:%s' ) as update_time from fa_train
""")
{
  "properties" : { },
  "id" : "17fc89bcccde4fbc8155a71f9c548223",
  "script" : null,
  "groupId" : "bac6f989d5f94a149eeac4bad9ff0b24",
  "name" : "查询",
  "createTime" : null,
  "updateTime" : 1698423106418,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "list",
  "method" : "GET",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": 1,\n    \"message\": \"success\",\n    \"data\": [\n        {\n            \"id\": 1,\n            \"number\": \"1\",\n            \"stationName\": \"1\",\n            \"operationTime\": \"1\",\n            \"reportTime\": \"12:00:00\",\n            \"createtime\": \"2023-10-27 14:37:54\",\n            \"updatetime\": \"2023-10-28 14:38:00\"\n        },\n        {\n            \"id\": 2,\n            \"number\": \"1\",\n            \"stationName\": \"1\",\n            \"operationTime\": \"1\",\n            \"reportTime\": \"12:00:00\",\n            \"createtime\": \"2023-10-27 14:37:54\",\n            \"updatetime\": \"2023-10-28 14:38:00\"\n        },\n        {\n            \"id\": 3,\n            \"number\": \"1\",\n            \"stationName\": \"1\",\n            \"operationTime\": \"1\",\n            \"reportTime\": \"12:00:00\",\n            \"createtime\": null,\n            \"updatetime\": null\n        },\n        {\n            \"id\": 4,\n            \"number\": \"1\",\n            \"stationName\": \"1\",\n            \"operationTime\": \"1\",\n            \"reportTime\": \"12:00:00\",\n            \"createtime\": \"2023-10-27 14:47:49\",\n            \"updatetime\": null\n        },\n        {\n            \"id\": 5,\n            \"number\": \"1\",\n            \"stationName\": \"1\",\n            \"operationTime\": \"1\",\n            \"reportTime\": \"12:00:00\",\n            \"createtime\": \"2023-10-27 14:47:50\",\n            \"updatetime\": null\n        }\n    ],\n    \"timestamp\": 1698391017758,\n    \"executeTime\": 2\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.select("""
    select `id`, `number`, `station_name`, `operation_time`, 
    `report_time`,DATE_FORMAT(create_time, '%Y-%m-%d %H:%i:%s' ) as create_time,
    DATE_FORMAT(update_time, '%Y-%m-%d %H:%i:%s' ) as update_time from fa_train
""");
{
  "properties" : { },
  "id" : "915ba6b4d03341e08fec34117d62a988",
  "script" : null,
  "groupId" : "0ed04b44d57948e9adf361efac29c000",
  "name" : "保存",
  "createTime" : null,
  "updateTime" : 1698420930030,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "save",
  "method" : "POST",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "{\r\n    \"name\":\"12\"\r\n}",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": -1,\n    \"message\": \"系统内部出现错误\",\n    \"data\": null,\n    \"timestamp\": 1698393171834,\n    \"executeTime\": 5\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.insert(
    """
   INSERT INTO `fa_remind`.`fa_users`(`name`,`type`,`phone`, `create_time`) 
   VALUES (#{body.name},#{body.type},#{body.phone}, now());
    """
)
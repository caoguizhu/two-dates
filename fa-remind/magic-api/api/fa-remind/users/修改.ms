{
  "properties" : { },
  "id" : "f3aa08146d7e4e1ca41759139764d446",
  "script" : null,
  "groupId" : "0ed04b44d57948e9adf361efac29c000",
  "name" : "修改",
  "createTime" : null,
  "updateTime" : 1698420597565,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "update",
  "method" : "PUT",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : null,
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.update(
    """
   UPDATE `fa_remind`.`fa_users` 
   SET  `update_time` =now()
    ?{body.name, ,`name` = #{body.name}}
     ?{body.type, ,`type` = #{body.type}}
      ?{body.phone, ,`phone` = #{body.phone}}
   WHERE `id` = #{body.id};
    """
)
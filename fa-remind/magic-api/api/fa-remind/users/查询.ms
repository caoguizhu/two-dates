{
  "properties" : { },
  "id" : "57e875bbd35c4254804da3575a1b9f2c",
  "script" : null,
  "groupId" : "0ed04b44d57948e9adf361efac29c000",
  "name" : "查询",
  "createTime" : null,
  "updateTime" : 1698428469362,
  "lock" : null,
  "createBy" : null,
  "updateBy" : null,
  "path" : "list",
  "method" : "GET",
  "parameters" : [ ],
  "options" : [ ],
  "requestBody" : "",
  "headers" : [ ],
  "paths" : [ ],
  "responseBody" : "{\n    \"code\": -1,\n    \"message\": \"系统内部出现错误\",\n    \"data\": null,\n    \"timestamp\": 1698428440361,\n    \"executeTime\": 35\n}",
  "description" : null,
  "requestBodyDefinition" : null,
  "responseBodyDefinition" : null
}
================================
return db.select("""
    select `id`, `name`,`type`,`phone`,DATE_FORMAT(create_time, '%Y-%m-%d %H:%i:%s' ) as create_time,
    DATE_FORMAT(update_time, '%Y-%m-%d %H:%i:%s' ) as update_time from fa_users 
    where  1=1
    ?{type , and type = #{type}}
""");
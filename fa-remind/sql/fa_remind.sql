/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : fa_remind

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 27/10/2023 16:27:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fa_busi
-- ----------------------------
DROP TABLE IF EXISTS `fa_busi`;
CREATE TABLE `fa_busi`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `train_id` int(0) NULL DEFAULT NULL COMMENT '车次id',
  `user_id` int(0) NULL DEFAULT NULL COMMENT '人员id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  `reality_time` datetime(0) NULL DEFAULT NULL COMMENT '实际汇报时间',
  `phone_remind_time` datetime(0) NULL DEFAULT NULL COMMENT '值班室电话提醒时间',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人员名称',
  `train_number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车次',
  `station_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车站名称',
  `operation_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '到开站时间',
  `report_time` time(0) NULL DEFAULT NULL COMMENT '规定汇报时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_busi
-- ----------------------------
INSERT INTO `fa_busi` VALUES (1, 1, 1, '2023-10-27 15:57:51', '2023-10-27 16:23:06', '2023-12-13 12:00:00', '2023-12-13 12:00:00', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (2, 1, 1, '2023-10-27 16:01:58', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (3, 1, 1, '2023-10-27 16:07:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (4, 1, 1, '2023-10-27 16:07:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (5, 1, 1, '2023-10-27 16:07:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (6, 1, 1, '2023-10-27 16:07:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (7, 1, 1, '2023-10-27 16:07:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (8, 1, 1, '2023-10-27 16:07:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (9, 1, 1, '2023-10-27 16:07:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (10, 1, 1, '2023-10-27 16:07:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (11, 1, 1, '2023-10-27 16:07:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (12, 1, 1, '2023-10-27 16:07:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (13, 1, 1, '2023-10-27 16:07:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (14, 1, 1, '2023-10-27 16:07:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (15, 1, 1, '2023-10-27 16:07:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (16, 1, 1, '2023-10-27 16:07:07', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (17, 1, 1, '2023-10-27 16:07:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (18, 1, 1, '2023-10-27 16:07:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (19, 1, 1, '2023-10-27 16:07:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (20, 1, 1, '2023-10-27 16:07:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (21, 1, 1, '2023-10-27 16:07:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (22, 1, 1, '2023-10-27 16:07:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (23, 1, 1, '2023-10-27 16:07:13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (24, 1, 1, '2023-10-27 16:07:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (25, 1, 1, '2023-10-27 16:07:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (26, 1, 1, '2023-10-27 16:07:14', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `fa_busi` VALUES (27, 1, 1, '2023-10-27 16:20:03', NULL, NULL, NULL, '12', '12', '12', '12', '00:00:12');

-- ----------------------------
-- Table structure for fa_train
-- ----------------------------
DROP TABLE IF EXISTS `fa_train`;
CREATE TABLE `fa_train`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车次',
  `station_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作业站名称',
  `operation_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '到开站时间',
  `report_time` time(0) NULL DEFAULT NULL COMMENT '规定汇报时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_train
-- ----------------------------
INSERT INTO `fa_train` VALUES (1, '1', '1', '1', '12:00:00', '2023-10-27 14:37:54', '2023-10-28 14:38:00');
INSERT INTO `fa_train` VALUES (2, '1', '1', '1', '12:00:00', '2023-10-27 14:37:54', '2023-10-28 14:38:00');
INSERT INTO `fa_train` VALUES (3, '1', '1', '1', '12:00:00', NULL, NULL);
INSERT INTO `fa_train` VALUES (4, '1', '1', '1', '12:00:00', '2023-10-27 14:47:49', NULL);
INSERT INTO `fa_train` VALUES (5, '1', '1', '1', '12:00:00', '2023-10-27 14:47:50', NULL);
INSERT INTO `fa_train` VALUES (6, '1', '1', '1', '12:00:00', '2023-10-27 15:28:25', NULL);
INSERT INTO `fa_train` VALUES (7, '1', '1', '1', '12:00:00', '2023-10-27 15:28:28', NULL);
INSERT INTO `fa_train` VALUES (8, '1', ' 2', '3', '00:00:04', '2023-10-27 15:41:35', NULL);

-- ----------------------------
-- Table structure for fa_users
-- ----------------------------
DROP TABLE IF EXISTS `fa_users`;
CREATE TABLE `fa_users`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '人员信息',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fa_users
-- ----------------------------
INSERT INTO `fa_users` VALUES (1, '1', '2023-10-27 15:31:48', '2023-10-27 15:31:44');
INSERT INTO `fa_users` VALUES (2, '1', '2023-10-27 15:34:10', NULL);
INSERT INTO `fa_users` VALUES (3, '12', '2023-10-27 15:53:07', NULL);

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test`  (
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `id` int(0) NOT NULL,
  `date` datetime(0) NULL DEFAULT NULL,
  `kk_jj` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES ('111', 1, '2023-10-27 14:51:34', NULL);

SET FOREIGN_KEY_CHECKS = 1;

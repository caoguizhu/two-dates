/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : fa_remind

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 27/10/2023 14:53:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for train_number
-- ----------------------------
DROP TABLE IF EXISTS `train_number`;
CREATE TABLE `train_number`  (
  `id` int(0) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `number` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '车次',
  `station_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '作业站名称',
  `operation_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '到开站时间',
  `report_time` time(0) NULL DEFAULT NULL COMMENT '规定汇报时间',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of train_number
-- ----------------------------
INSERT INTO `train_number` VALUES (1, '1', '1', '1', '12:00:00', '2023-10-27 14:37:54', '2023-10-28 14:38:00');
INSERT INTO `train_number` VALUES (2, '1', '1', '1', '12:00:00', '2023-10-27 14:37:54', '2023-10-28 14:38:00');
INSERT INTO `train_number` VALUES (3, '1', '1', '1', '12:00:00', NULL, NULL);
INSERT INTO `train_number` VALUES (4, '1', '1', '1', '12:00:00', '2023-10-27 14:47:49', NULL);
INSERT INTO `train_number` VALUES (5, '1', '1', '1', '12:00:00', '2023-10-27 14:47:50', NULL);

SET FOREIGN_KEY_CHECKS = 1;

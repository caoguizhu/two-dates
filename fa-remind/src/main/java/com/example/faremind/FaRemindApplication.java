package com.example.faremind;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FaRemindApplication {

    public static void main(String[] args) {
        SpringApplication.run(FaRemindApplication.class, args);
    }

}

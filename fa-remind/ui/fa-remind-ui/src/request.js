// 引入axios
import axios from 'axios'
import { ElMessage, ElMessageBox } from "element-plus";
// 创建实例
let instance = axios.create({
    timeout: 15000  // 毫秒
})
instance.interceptors.response.use(function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    console.log(response)
    if(response.status == 200 && response.data.code == 1){
        return response;
    }else{
        ElMessage({
            type: "error",
            message: "服务器错误",
          });
        throw new Error()
    }
  }, function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    ElMessage({
        type: "error",
        message: "服务器错误",
      });
    return Promise.reject(error);
  });
export default instance;
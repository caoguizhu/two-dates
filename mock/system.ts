// test.ts

import { MockMethod } from "vite-plugin-mock";
export default [
  {
    url: "/mock/login",
    method: "post",
    response: ({ query }) => {
      return {
        code: 0,
        data: {
          name: "vben",
        },
      };
    },
  },
  {
    url: "/api/system/getMenu",
    method: "get",
    timeout: 2000,
    response: {
      code: 200,
      data: {
        list: [
          {
            path: "/system",
            name: "system",
            id:'1',
            meta: {
              title: "系统管理",
            },
            children: [
              {
                path: "/system/user",
                name: "user",
                id:'1-1',
                meta: {
                  title: "用户管理",
                },
              },
              {
                path: "/system/menu",
                name: "menu",
                id:'1-2',
                meta: {
                  title: "菜单管理",
                },
              },
              {
                path: "/system/role",
                name: "role",
                id:'1-3',
                meta: {
                  title: "角色管理",
                },
              },
            ],
          },
        ],
      },
    },
  },
] as MockMethod[];

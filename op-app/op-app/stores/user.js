import {
	defineStore
} from 'pinia';

export const useUserInfoStore = defineStore('userInfo', {
	state: () => {
		return {
			userInfo: uni.getStorageSync("userInfo") || {},
			token:uni.getStorageSync("token") || null,
			fillInfo:uni.getStorageSync("fillInfo") || {},
		}
	},
	getters:{
		userInfos:state=>state.userInfo,
		userToken:state=>state.token,
		userFillInfo:state=>state.fillInfo,
	},
	actions:{
		setUserInfo(userInfo){
			console.log(userInfo)
			this.userInfo = userInfo;
			uni.setStorageSync("userInfo",this.userInfo)
		},
		setToken(token){
			console.log(token)
			this.token = token;
			uni.setStorageSync("token",this.token)
		},
		setFillInfo(fillInfo){
			console.log(fillInfo)
			this.fillInfo = fillInfo;
			uni.setStorageSync("fillInfo",this.fillInfo)
		},
		logout(){
			this.userInfo ={};
			this.token="";
			uni.setStorageSync("token",this.token)
			uni.setStorageSync("userInfo",this.userInfo)
		}
	}
});
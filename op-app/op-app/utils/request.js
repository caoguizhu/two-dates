import {
		useUserInfoStore
	} from '../stores/user.js'
let baseurl = "http://36.133.253.217/op"
// #ifdef H5
baseurl = "/op"
// #endif
console.log(baseurl)
export function request({
	url,
	method,
	data,
	params,
}) {

	return new Promise((resolve, recject) => {
		try {
			uni.request({
				url:baseurl+url,
				method: method,
				data: data,
				header: {
					token: uni.getStorageSync("token")
				},
				success(response) {
					if(response.data.code == 402){
						uni.showToast({
							title:data.message,
							icon:"none"
						})
						setTimeout(()=>{
							toLogin();
						},1000)
						recject(false)
					}
					console.log(response)
					if (response.data.code == 200) {
						resolve(response)
					} else {
						const msg = response.data.message || "服务器错误"
						uni.showToast({
							title: msg,
							icon:"none"
						})
						recject(response)
					}

				},
				fail(err) {
					recject(err)
				}
			})
		} catch (e) {
			console.log(e)
		}

	})
}
export function upload({
	url,
	filePath,
	type
}) {
	console.log(filePath)
	return new Promise((reslove, resject) => {
		uni.uploadFile({
			url: baseurl + url,
			filePath: filePath,
			name:"file",
			success(e) {
					console.log(e)
				let data = JSON.parse(e.data)
				if(data.code == 402){
					uni.showToast({
						title:data.message,
						icon:"none"
					})
					setTimeout(()=>{
						toLogin();
					},1000)
					resject(false)
				}
				if (data.code == 200) {
					data.result.type = type
					reslove(data.result)
				} else {
					uni.showToast({
						title:data.message,
						icon:"none"
					})
					resject(false)
				}

			},
			fail() {
				resject(false)
			}
		})
	})
}

function toLogin() {
	const users = useUserInfoStore();
	users.setToken('');
	users.setUserInfo({})
	uni.reLaunch({
		url: '/pages/login/login'
	})
}
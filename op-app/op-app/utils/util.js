//校验邮箱
export function validEmail(email){
	return /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/.test(email)
}

export function getTime(){
	let currDate = new Date();
	// 将日期格式化为年月日时分秒字符串
	let formattedDate = `${currDate.getFullYear()}-${addZero(currDate.getMonth() + 1)}-${addZero(currDate.getDate())} ${addZero(currDate.getHours())}:${addZero(currDate.getMinutes())}:${addZero(currDate.getSeconds())}`;
	// 输出格式化后的日期
	return formattedDate;
}

// 补0函数
function addZero(num) {
  return num < 10 ? `0${num}` : num;
}
export function formatFileSize(bytes) {
	const units = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
	let size = bytes;
	let unitIndex = 0;
	while (size >= 1024 && unitIndex < units.length - 1) {
		size /= 1024;
		unitIndex++;
	} // 保留两位小数，四舍五入size = Math.round(size * 100) / 100;
	return `${size} ${units[unitIndex]}`;
}
import { defineConfig } from "vite"
import uni from "@dcloudio/vite-plugin-uni"; // 注意此处，特别重要
  
export default defineConfig({
	plugins: [
		uni()
	],
	server: {
		proxy: {
			'/op': {
				target: 'http://36.133.253.217',
				changeOrigin: true
			}, 
			'/api': {
				target: 'http://172.16.80.252:8080',
				changeOrigin: true,
				rewrite:path=>path.replace(/^\/api/,'')
			},
		}
	}
})

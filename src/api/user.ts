import request from "@/utils/request"
export function getUserMenu(data?: any) {
    return request({
      method: "GET",
      url: `/api/system/getMenu`,
      data: data,
    });
  }
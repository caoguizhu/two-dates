export const config = {
    theme: {
        token:{
            borderRadius: 3,
            wireframe: true,
            sizeUnit: 3,
        }
    }
}
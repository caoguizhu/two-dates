import { createApp } from "vue";
import App from "./App.vue";
import { setupRouter } from "@/router";
import { setupStore } from "@/store";
import "@/styles/style.css";
import "@/styles/tailwind.css";
async function bootstrap() {
  const app = createApp(App);
  // Configure routing
  // 配置路由
  setupRouter(app);
  // Configure store
  // 配置 store
  setupStore(app);
  app.mount("#app");
}
bootstrap();

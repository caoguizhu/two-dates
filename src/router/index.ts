import { createRouter, createWebHistory } from "vue-router";
import { staticRouter } from "./pages/static-router";
import type { App } from "vue";

export const router = createRouter({
  history: createWebHistory(),
  routes: staticRouter,
});
// config router
// 配置路由器
export function setupRouter(app: App<Element>) {
    app.use(router);
}

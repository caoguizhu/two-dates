import LAYOUT from "@/pages/index.vue";
export const staticRouter = [
  {
    path: "/",
    name: "Root",
    meta: {
      title: "Root",
    },
    component: LAYOUT,
    redirect: "/welcome",
    children: [
      {
        path:"welcome",
        name:"welcome",
        meta: {
          title: "首页",
        },
        component: () => import("@/views/welcome.vue")
      }
    ]
  },

];

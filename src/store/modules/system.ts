import { defineStore } from "pinia";
import { getCssVar,setCssVar } from "@/utils/util";
import {config} from "@/config/system";
export const useSystemStore = defineStore("system", {
  state: () => {
    return {
      collapsed: false,
      theme:{
        token:{
          ...config.theme.token,
          colorPrimary: getCssVar("--primary-color"),
        }
      }
    };
  },
  getters: {},
  actions: {
    SET_COLLAPSED(collapsed: boolean) {
      this.collapsed = collapsed;
    },
    SET_PRIMARY_COLOR(color: string) {
       setCssVar("--primary-color",color);
       this.theme.token.colorPrimary = color;
    }
  },
});

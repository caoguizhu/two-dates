import { defineStore } from "pinia";
import {getUserMenu} from "@/api/user"

export const useUserStore = defineStore("user", {
  state: () => {
    return {
       menu:[] as any
    };
  },
  getters: {},
  actions: {
    getUserMenu(){
        getUserMenu().then((res:any)=>{
           this.menu = res.list
        })
    }
  },
});

import { useUserStore } from "@/store/modules/user";
import { store } from "@/store";
import type { App } from "vue";
const useUser: any = useUserStore(store);
// 权限
const hasPermission = {
  install(app: any) {
    app.directive("permission", {
      mounted(el: any, binding: any) {
        if (binding.value) {
          const permissionList = useUser.userInfo.authorities || [];
          if (
            permissionList &&
            !permissionList.includes(binding.value)
          ) {
            el.remove();
          }
        }
      },
    });
  },
};

export function setupPermission(app: App<Element>) {
  app.use(hasPermission);
}

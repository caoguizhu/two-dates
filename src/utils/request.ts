import axios from "axios";
import type {
  AxiosInstance,
  AxiosResponse,
  InternalAxiosRequestConfig,
  AxiosError,
} from "axios";
import { notification } from "ant-design-vue";
import { useUserStore } from "@/store/modules/user";
import { store } from "@/store";
import { router } from "@/router";

const axiosInstance: AxiosInstance = axios.create({
  timeout: 30 * 1000, // 超时时间
});

// axios实例拦截请求
axiosInstance.interceptors.request.use(
  (config: InternalAxiosRequestConfig) => {
    const useUser: any = useUserStore(store);
    config.headers.token = useUser.token;
    return config;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  }
);
// axios实例拦截响应
axiosInstance.interceptors.response.use(
  (response: AxiosResponse) => {
    if (response.status === 200 && response.data.code === 200) {
      const data = response.data.data;
      return data  as any;
    } else {
      errorAxios(response.data);
      return Promise.reject(response);
    }
  },
  // 请求失败
  (error: AxiosError) => {
    const { response } = error;
    const errorParams = {
      code: response?.status,
      message: response?.statusText,
    };
    errorAxios(errorParams as any);
    return Promise.reject(error);
  }
);

//错误处理
function errorAxios(err: any): void {
  const { code, message } = err;
  errMessageTip(message);
  if (code == 401 || code == 402 || code == 403) {
   
    const useUser: any = useUserStore(store);
    useUser.outLogin(true).then(() => {
      //刷新退出
       router.push({ path: "/login" });
       location.reload();
    });
  }
}
// 消息提示
function errMessageTip(message: string): void {
  notification.error({
    message: "错误提示",
    description: message || "服务错误,请联系管理员！",
  });
}
export default axiosInstance;

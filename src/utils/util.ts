export function fullScreen():void{
    console.log("fullScreen");
}
/**
 * 获取css 变量
 */
export function getCssVar(name:string):string|null{
    return getComputedStyle(document.documentElement).getPropertyValue(name);
}
/**
 * 设置css 变量
 */
export function setCssVar(name:string,color:string):void{
     document.documentElement.style.setProperty(name, color);
}